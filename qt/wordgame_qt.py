# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wordgame.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import sys
import trans
import random
import pickle

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

def check_if_input_valid(input):
    print 'input: %s' % input
    return len(input) > 0

class UiMainWindow(QtGui.QMainWindow):
    def setup_ui(self):
        self.setup_base_frame()
        self.setup_start_page()
        self.setup_game_page()
        self.setup_results_page()
        self.program_buttons()

        self.retranslateUi()
        self.stacked_widget.setCurrentIndex(0)  # which page
        QtCore.QMetaObject.connectSlotsByName(self)

    def setup_base_frame(self):
        self.setObjectName(_fromUtf8("self"))
        #self.resize(371, 337)
        self.setFixedSize(371, 337)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("wordgameicon.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(self)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.stacked_widget = QtGui.QStackedWidget(self.centralwidget)
        self.stacked_widget.setGeometry(QtCore.QRect(0, 10, 361, 301))
        self.stacked_widget.setObjectName(_fromUtf8("stacked_widget"))

        self.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 371, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuView = QtGui.QMenu(self.menubar)
        self.menuView.setObjectName(_fromUtf8("menuView"))
        self.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(self)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        self.setStatusBar(self.statusbar)
        self.act_new = QtGui.QAction(self)
        self.act_new.setObjectName(_fromUtf8("act_new"))
        self.act_results = QtGui.QAction(self)
        self.act_results.setObjectName(_fromUtf8("act_results"))
        self.act_exit = QtGui.QAction(self)
        self.act_exit.setObjectName(_fromUtf8("act_exit"))
        self.menuFile.addAction(self.act_new)
        self.menuFile.addAction(self.act_exit)
        self.menuView.addAction(self.act_results)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuView.menuAction())

    def setup_start_page(self):
        self.page = QtGui.QWidget()
        self.page.setObjectName(_fromUtf8("page"))
        self.lbl_give_name = QtGui.QLabel(self.page)
        self.lbl_give_name.setGeometry(QtCore.QRect(20, 10, 321, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Tahoma"))
        font.setPointSize(12)
        self.lbl_give_name.setFont(font)
        self.lbl_give_name.setObjectName(_fromUtf8("lbl_give_name"))
        self.txt_name = QtGui.QLineEdit(self.page)
        self.txt_name.setGeometry(QtCore.QRect(20, 50, 231, 20))
        self.txt_name.setObjectName(_fromUtf8("txt_name"))
        self.btn_start_exit = QtGui.QPushButton(self.page)
        self.btn_start_exit.setGeometry(QtCore.QRect(250, 250, 75, 23))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.btn_start_exit.setFont(font)
        self.btn_start_exit.setObjectName(_fromUtf8("btn_start_exit"))
        self.btn_start = QtGui.QPushButton(self.page)
        self.btn_start.setGeometry(QtCore.QRect(20, 250, 75, 23))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.btn_start.setFont(font)
        self.btn_start.setObjectName(_fromUtf8("btn_start"))
        self.stacked_widget.addWidget(self.page)

    def setup_game_page(self):
        self.page_2 = QtGui.QWidget()
        self.page_2.setObjectName(_fromUtf8("page_2"))
        self.rb_choice5 = QtGui.QRadioButton(self.page_2)
        self.rb_choice5.setGeometry(QtCore.QRect(20, 190, 321, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.rb_choice5.setFont(font)
        self.rb_choice5.setObjectName(_fromUtf8("rb_choice5"))
        self.lbl_meaning = QtGui.QLabel(self.page_2)
        self.lbl_meaning.setGeometry(QtCore.QRect(20, 40, 321, 61))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.lbl_meaning.setFont(font)
        self.lbl_meaning.setTextFormat(QtCore.Qt.AutoText)
        self.lbl_meaning.setObjectName(_fromUtf8("lbl_meaning"))
        self.lbl_meaning.setWordWrap(True);
        self.rb_choice2 = QtGui.QRadioButton(self.page_2)
        self.rb_choice2.setGeometry(QtCore.QRect(20, 130, 321, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.rb_choice2.setFont(font)
        self.rb_choice2.setObjectName(_fromUtf8("rb_choice2"))
        self.rb_choice3 = QtGui.QRadioButton(self.page_2)
        self.rb_choice3.setGeometry(QtCore.QRect(20, 150, 321, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.rb_choice3.setFont(font)
        self.rb_choice3.setObjectName(_fromUtf8("rb_choice3"))
        self.lbl_task = QtGui.QLabel(self.page_2)
        self.lbl_task.setGeometry(QtCore.QRect(20, 10, 321, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Tahoma"))
        font.setPointSize(12)
        self.lbl_task.setFont(font)
        self.lbl_task.setObjectName(_fromUtf8("lbl_task"))
        self.rb_choice4 = QtGui.QRadioButton(self.page_2)
        self.rb_choice4.setGeometry(QtCore.QRect(20, 170, 321, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.rb_choice4.setFont(font)
        self.rb_choice4.setObjectName(_fromUtf8("rb_choice4"))
        self.rb_choice1 = QtGui.QRadioButton(self.page_2)
        self.rb_choice1.setGeometry(QtCore.QRect(20, 110, 321, 17))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.rb_choice1.setFont(font)
        self.rb_choice1.setObjectName(_fromUtf8("rb_choice1"))
        self.btn_send = QtGui.QPushButton(self.page_2)
        self.btn_send.setGeometry(QtCore.QRect(20, 250, 75, 23))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.btn_send.setFont(font)
        self.btn_send.setObjectName(_fromUtf8("btn_send"))
        self.btn_game_exit = QtGui.QPushButton(self.page_2)
        self.btn_game_exit.setGeometry(QtCore.QRect(250, 250, 75, 23))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.btn_game_exit.setFont(font)
        self.btn_game_exit.setObjectName(_fromUtf8("btn_game_exit"))
        self.stacked_widget.addWidget(self.page_2)

        self.group = QtGui.QButtonGroup()
        self.group.addButton(self.rb_choice1)
        self.group.addButton(self.rb_choice2)
        self.group.addButton(self.rb_choice3)
        self.group.addButton(self.rb_choice4)
        self.group.addButton(self.rb_choice5)

    def setup_results_page(self):
        self.page_3 = QtGui.QWidget()
        self.page_3.setObjectName(_fromUtf8("page_3"))
        self.lbl_results = QtGui.QLabel(self.page_3)
        self.lbl_results.setGeometry(QtCore.QRect(20, 10, 321, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Tahoma"))
        font.setPointSize(12)
        self.lbl_results.setFont(font)
        self.lbl_results.setObjectName(_fromUtf8("lbl_results"))
        #self.lst_results = QtGui.QListView(self.page_3)
        self.lst_results = QtGui.QListWidget(self.page_3)
        self.lst_results.setGeometry(QtCore.QRect(20, 50, 301, 181))
        self.lst_results.setObjectName(_fromUtf8("lst_results"))
        self.btn_result_exit = QtGui.QPushButton(self.page_3)
        self.btn_result_exit.setGeometry(QtCore.QRect(250, 250, 75, 23))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.btn_result_exit.setFont(font)
        self.btn_result_exit.setObjectName(_fromUtf8("btn_result_exit"))
        self.btn_new = QtGui.QPushButton(self.page_3)
        self.btn_new.setGeometry(QtCore.QRect(20, 250, 75, 23))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(10)
        self.btn_new.setFont(font)
        self.btn_new.setObjectName(_fromUtf8("btn_new"))
        self.stacked_widget.addWidget(self.page_3)

    def retranslateUi(self):
        self.setWindowTitle(_translate("self", "Word Game", None))
        self.lbl_give_name.setText(_translate("self", "Give me your name", None))
        self.btn_start_exit.setText(_translate("self", "Exit", None))
        self.btn_start.setText(_translate("self", "Start", None))
        self.rb_choice5.setText(_translate("self", "fifth", None))
        self.lbl_meaning.setText(_translate("self", "The meaning of the word is described here", None))
        self.rb_choice2.setText(_translate("self", "second", None))
        self.rb_choice3.setText(_translate("self", "third", None))
        self.lbl_task.setText(_translate("self", "Choose the right word for the given meaning", None))
        self.rb_choice4.setText(_translate("self", "forth", None))
        self.rb_choice1.setText(_translate("self", "first", None))
        self.btn_send.setText(_translate("self", "Send", None))
        self.btn_game_exit.setText(_translate("self", "Exit", None))
        self.lbl_results.setText(_translate("self", "Results", None))
        self.btn_result_exit.setText(_translate("self", "Exit", None))
        self.btn_new.setText(_translate("self", "New", None))
        self.menuFile.setTitle(_translate("self", "File", None))
        self.menuView.setTitle(_translate("self", "View", None))
        self.act_new.setText(_translate("self", "New", None))
        self.act_results.setText(_translate("self", "Results", None))
        self.act_exit.setText(_translate("self", "Exit", None))

    def program_buttons(self):
        self.connect(self.btn_game_exit, QtCore.SIGNAL('clicked()'), lambda: self.confirm_to_interrupt_game('Are you sure you want to interrupt your current game and quit?', self.close))
        self.connect(self.btn_start_exit, QtCore.SIGNAL('clicked()'), self.close)
        self.connect(self.btn_result_exit, QtCore.SIGNAL('clicked()'), self.close)
        self.connect(self.btn_new, QtCore.SIGNAL('clicked()'), self.new_game)
        self.connect(self.btn_start, QtCore.SIGNAL('clicked()'), self.start_game)
        self.connect(self.btn_send, QtCore.SIGNAL('clicked()'), self.send_steps)
        self.act_new.triggered.connect(lambda: self.confirm_to_interrupt_game('Are you sure you want to interrupt your current game to start a new one?', self.new_game))
        self.act_results.triggered.connect(lambda: self.confirm_to_interrupt_game('Are you sure you want to interrupt your current game to see the results?', self.show_results))
        self.act_exit.triggered.connect(lambda: self.confirm_to_interrupt_game('Are you sure you want to interrupt your current game and quit?', self.close))

    def confirm_to_interrupt_game(self, message, func):
        if hasattr(self, 'player_name') and self.player_name:
            reply = QtGui.QMessageBox.question(self, 'Question', 
                         message, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                func()
        else:
            func()

    def new_game(self):
        self.stacked_widget.setCurrentIndex(0)

    def start_game(self):
        if self.txt_name.text():
            self.good_answers_num = 0
            self.player_name = str(self.txt_name.text())
            self.game_round = business.start_game(self.player_name)
            self.set_game_page_widgets()
            if self.lst_results.count() == 0:
                self.add_items_to_results_list(business.results)
            self.stacked_widget.setCurrentIndex(1)
        else:
            QMessageBox.critical(self, "Error", "Name must be given!")

    def set_game_page_widgets(self):
        self.clean_up_radiobuttons()
        self.round = next(self.game_round())
        print 'self.round: %s' % str(self.round)
        self.lbl_meaning.setText(_translate("self", self.round['meaning'], None))
        for i in range(len(self.round['choices'])):
            exec('self.rb_choice%s.setText(_translate("self", self.round["choices"][%s], None))' % (i + 1,i))

    def clean_up_radiobuttons(self):
        self.group.setExclusive(False)
        for i in range(4):
            exec('self.rb_choice%s.setChecked(False)' % str(i + 1))
        self.group.setExclusive(True)

    def send_steps(self):
        if self.check_if_one_selected():
            if self.get_good_button(self.round['answer']).isChecked():
                QMessageBox.information(self, 'Congratulation', 'YES! %s is the right answer' % self.round['answer'])
                self.good_answers_num += 1
                self.set_game_page_widgets()
            else:
                QMessageBox.information(self, 'Game Over', 'NOOO! wrong answer, the right one is %s. You had %s good answers' % (self.round['answer'], self.good_answers_num))
                business.end_game(int(self.good_answers_num))
                self.add_items_to_results_list([(self.player_name, self.good_answers_num)])
                self.player_name = None
                self.confirm_to_interrupt_game('Are you sure you want to interrupt your current game?', self.show_results)
        else:
            QMessageBox.warning(self, "Error", "You have not choosen any options.")

    def check_if_one_selected(self):
        return any((self.rb_choice1.isChecked(),
                self.rb_choice2.isChecked(),
                self.rb_choice3.isChecked(),
                self.rb_choice4.isChecked(),
                self.rb_choice5.isChecked()))
    
    def add_items_to_results_list(self, results):
        for name, score in results:
            QtGui.QListWidgetItem("%s %s" % (name.ljust(20), score), self.lst_results)

    def show_results(self):
        self.lst_results.show()
        self.stacked_widget.setCurrentIndex(2)

    def get_good_button(self, answer):
        print 'answer %s' % type(answer)
        return {
            str(self.rb_choice1.text()) : self.rb_choice1,
            str(self.rb_choice2.text()) : self.rb_choice2,
            str(self.rb_choice3.text()) : self.rb_choice3,
            str(self.rb_choice4.text()) : self.rb_choice4,
            str(self.rb_choice5.text()) : self.rb_choice5
        }[answer]

class Business(object):
    def __init__(self, *files):
        self.words = list()
        self.played_words = list()
        self._results = Results()
        for file in files:
            with open(file) as f:
                self.words.extend(f.read().split('\n'))

    @property
    def results(self):
        return self._results.results

    def start_game(self, player_name):
        self.player_name = player_name
        return self.game_round

    def game_round(self):
        '''
        {'meaning': 'testvalue', 'answer': 'testword', 'choices': ('1','2','testword', '4')}
        '''
        while True:
            answer = random.choice(self.words)
            while answer in self.played_words:
                answer = random.choice(self.words)
            meaning = trans.get_meaning(answer)
            choices = [random.choice(self.words), random.choice(self.words), random.choice(self.words), random.choice(self.words)]
            while answer in choices and any(c in self.played_words for c in choices):
                choices = [random.choice(self.words), random.choice(self.words), random.choice(self.words), random.choice(self.words)]
            choices.append(answer)
            random.shuffle(choices)
            yield dict(meaning=meaning, answer=answer, choices=choices)

    def end_game(self, good_answers_num):
        self.results.append((self.player_name, good_answers_num))
        self._results.update_saved_results()
        del self.played_words[:]

class Results(object):
    def __new__(cls, *args, **kwargs):
        try:
            with open('saved_results.dat', 'rb') as file1:
                obj = pickle.load(file1)
                if isinstance(obj, Results):
                    return obj
                else:
                    print('this is not Results')
                    return super(Results, cls).__new__(cls, *args, **kwargs)
        except IOError:
            print('there is no file for saved data')
            return super(Results, cls).__new__(cls, *args, **kwargs)
        except EOFError:
            print('no data could be gotten out of the saved data file')
            return super(Results, cls).__new__(cls, *args, **kwargs)

    def __init__(self):
        if not hasattr(self, 'results'):
            self.results = list()

    def update_saved_results(self):
        try:
            with open('saved_results.dat', 'wb+') as f:
                pickle.dump(self, f)
        except IOError:
            print('there is no file for saved data')
        except EOFError:
            print('no data could be gotten out of the saved data file')

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    #self = QtGui.QMainWindow()
    business = Business('words.txt')
    ui = UiMainWindow()
    ui.setup_ui()
    ui.show()
    sys.exit(app.exec_())

