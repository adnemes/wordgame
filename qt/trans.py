from PyDictionary import PyDictionary
dic = PyDictionary()

def get_meaning(word):
    return_value = ''
    meaning = dic.meaning(word)
    if meaning.get('Noun',''):
        return_value += 'Noun: %s' % '; '.join(meaning.get('Noun',''))
    if meaning.get('Verb', ''):
        if return_value:
            return_value += '\n'
        return_value += 'Verb: %s' % '; '.join(meaning.get('Verb', ''))
    if meaning.get('Adjective', ''):
        if return_value:
            return_value += '\n'
        return_value += 'Adjective: %s' % '; '.join(meaning.get('Adjective', ''))
    return return_value

def get_synonym(word):
    synonym = dic.synonym(word)
    if synonym:
        return 'Synonyms: %s' % ', '.join(synonym)

def get_antonym(word):
    antonym = dic.antonym(word)
    if antonym:
        return 'Antonym: %s' % ', '.join(antonym)

def get_translation(word, language = 'hu'):
    translation = dic.translate(word, language)
    if translation:
        return 'Translation: %s' % translation

def run_on_words(file_path, *funcs):
    with open(file_path, 'r') as file:
        for line in file:
            word = line.strip()
            yield '\n' + word
            print(word)
            for func in funcs:
                try:
                    result = func(word)
                except:
                    result = None
                if result:
                    yield result

def print_words(processed_words, limit_num = None):
    for num, item in enumerate(processed_words):
        print(item)
        if limit_num and num >=limit_num:
            break

def writeout_words(file_path, processed_words):
    with open(file_path, 'w+') as file:
        for line in processed_words:
            file.write(line + '\n')

if __name__ == '__main__':
    processed_words = run_on_words('words2.txt', get_meaning, get_synonym)
    #print_words(processed_words)
    writeout_words('proc_words.txt', processed_words)