__version__ = "1.0"
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.actionbar import ActionBar
from kivy.uix.popup import Popup
from kivy.properties import StringProperty, ObjectProperty
from game import Game

Builder.load_file('wg.kv')

class HeadActionBar(ActionBar):
    def new_game(self):
        confirm_to_interrupt(
                            game.player_name != None,
                            'Are you sure you want to interrupt your current game to start a new one?',
                            lambda: (game.end_game(), main_screens['start_screen'].new_game())
                            )
    def show_results(self):
        confirm_to_interrupt(
                            game.player_name != None,
                            'Are you sure you want to interrupt your current game to see the results?',
                            lambda: (game.end_game(), main_screens['results_screen'].show_results())
                            )

    def exit(self):
        confirm_to_interrupt(
                            game.player_name !=None,
                            'Are you sure you want to interrupt your current game and quit?',
                            app.stop
                            )

class StartScreen(Screen):
    def new_game(self):
        switch_current_screen('start_screen')

    def start_game(self, player_name):
        try:
            game.start_game(player_name)
            main_screens['game_screen'].start_game()
        except ValueError as error_message:
            traceback.print_exc()
            MessagePopup(text=str(error_message)).open()

class GameScreen(Screen):
    choice_text_1 = StringProperty()
    choice_text_2 = StringProperty()
    choice_text_3 = StringProperty()
    choice_text_4 = StringProperty()
    meaning_text = StringProperty()
    example_text = StringProperty()

    def start_game(self):
        self.game_round_generator = game.get_game_rounds()
        self.do_game_round()

    def do_game_round(self):
        self.round = next(self.game_round_generator)
        self.set_items_on_game_round()
        switch_current_screen('game_screen')

    def set_items_on_game_round(self):
        self.choice_text_1, self.choice_text_2, self.choice_text_3, self.choice_text_4 = self.round['choices']
        self.meaning_text = self.round['meaning']
        self.example_text = self.round['example']

    def submit(self, player_answer):
        if player_answer == self.round['answer']:
            game.good_answers_num += 1
            MessagePopup(text='GOOD ONE!\nThat was your %s. good answer\n\nExample:\n%s' % (game.good_answers_num, self.round['full_example'])).open()
            self.do_game_round()
        else:
            popup = ConfirmPopup('Wrong answer the right one is: %s.\n\nExample:\n%s\n\n\nDo you want to start a new game?' % (self.round['answer'], self.round['full_example']), yes_func = lambda: main_screens['start_screen'].start_game(game.player_name), no_func = lambda: (game.end_game(), main_screens['results_screen'].show_results()))
            popup.open()

class ResultsScreen(Screen):
    results_list = ObjectProperty()
    def show_results(self):
        print game.results
        self.results_list.adapter.data = ['%s-%s' % (str(name).ljust(15), str(score).rjust(15)) for name, score in game.results]
        self.results_list._trigger_reset_populate()
        switch_current_screen('results_screen')

class MyApp(App):
    def build(self):
        self.title = 'Word game'
        return screen_manager

    def on_pause(self):
        # Here you can save data if needed
        return True

    def on_resume(self):
        # Here you can check if any data needs replacing (usually nothing)
        pass

class MessagePopup(Popup):
    def __init__(self, text, **kwargs):
        self.text = text
        super(MessagePopup,self).__init__(**kwargs)

class ConfirmPopup(Popup):
    title_text = 'Answer the question!'
    yes_text = 'Yes'
    no_text = 'No'

    def __init__(self, text, yes_func, no_func, **kwargs):
        self.text = text
        self.yes_func = yes_func
        self.no_func = no_func
        super(ConfirmPopup,self).__init__(**kwargs)

def add_screens_to_manager(screen_manager, **main_screens):
    for screen_name in main_screens:
        screen_manager.add_widget(main_screens[screen_name])

def switch_current_screen(screen_name):
    screen_manager.current = screen_name

def confirm_to_interrupt(interrupt_condition, message, yes_func, no_func = lambda: None):
    if interrupt_condition:
        print 'condition: %s' % interrupt_condition
        popup = ConfirmPopup(message, yes_func = yes_func, no_func = no_func)
        popup.open()
        print 'popup ended'
    else:
        yes_func()

if __name__ == '__main__':
    screen_manager = ScreenManager()
    try:
        #game = Game('/storage/sdcard0/Progs/processed_words.txt')
        game = Game('processed_words.txt')
        main_screens = dict(
                            start_screen = StartScreen(name='start_screen'),
                            game_screen = GameScreen(name='game_screen'),
                            results_screen = ResultsScreen(name='results_screen')
                            )
        add_screens_to_manager(screen_manager, **main_screens)
        screen_manager.current = 'start_screen'
    except IOError as error_message:
        traceback.print_exc()
        MessagePopup(text=str(error_message)).open()

    app = MyApp()
    app.run()