import unittest
import mock
from game import Game

class TestGame(unittest.TestCase):
    @mock.patch('game.Game.__init__')
    def setUp(self, init_mock):
        self.maxDiff = None
        init_mock.return_value = None
        self.test_game = Game()

    def test_get_game_rounds(self):
        pass

    def test_process_file_for_words_default(self):
        test_words = ['consider', 'deem to be', 'At the moment, artemisinin-based therapies are considered the best treatment, but cost about $10 per dose - far too much for impoverished communities.', 'minute', 'infinitely or immeasurably small', 'The minute stain on the document was not visible to the naked eye.']
        expected_result = {
        'consider': {
        'meaning': 'deem to be',
        'full_example': 'At the moment, artemisinin-based therapies are considered the best treatment, but cost about $10 per dose - far too much for impoverished communities.',
        'example': 'At the moment, artemisinin-based therapies are ... the best treatment, but cost about $10 per dose - far too much for impoverished communities.'},
        'minute': {
        'meaning': 'infinitely or immeasurably small',
        'example': 'The ... stain on the document was not visible to the naked eye.',
        'full_example': 'The minute stain on the document was not visible to the naked eye.'}
        }
        self.assertDictEqual(self.test_game.process_file_for_words(test_words), expected_result)

if __name__ == '__main__':
    unittest.main(verbosity = 2)