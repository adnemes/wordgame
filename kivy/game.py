import pickle
import random
import re

class Results(object):
    def __new__(cls, *args, **kwargs):
        try:
            with open('saved_results.dat', 'rb') as file1:
                obj = pickle.load(file1)
                if isinstance(obj, Results):
                    return obj
                else:
                    print('this is not Results')
                    return super(Results, cls).__new__(cls, *args, **kwargs)
        except IOError:
            print('there is no file for saved data')
            return super(Results, cls).__new__(cls, *args, **kwargs)
        except EOFError:
            print('no data could be gotten out of the saved data file')
            return super(Results, cls).__new__(cls, *args, **kwargs)

    def __init__(self):
        if not hasattr(self, 'results'):
            self.results = list()

    def update_saved_results(self):
        try:
            with open('saved_results.dat', 'wb+') as f:
                pickle.dump(self, f)
        except IOError:
            print('there is no file for saved data')
        except EOFError:
            print('no data could be gotten out of the saved data file')

class Game(object):
    def __init__(self, word_file_name):
        self._results = Results()
        self.player_name = None
        self.words = self.process_file_for_words(self.get_word_list(word_file_name))

    def get_word_list(self, word_file_name):
        with open(word_file_name) as file:
            return file.read().split('\n')

    def process_file_for_words(self, raw_word_list):
        '''
        1. word
        2. meaning
        3. example (optional)
        '''
        words = {}
        last_cought_word = ''
        for line in raw_word_list:
            line = line.strip()
            if ' ' not in line:
                words[line] = {}
                last_cought_word = line
            else:
                if words[last_cought_word].get('meaning', None):  #means we are over the meaning, next line is meaning then.
                    words[last_cought_word]['example'] = self.hide_word_in_example(last_cought_word, line)
                    words[last_cought_word]['full_example'] = line
                else:
                    words[last_cought_word]['meaning'] = line
        return words

    def hide_word_in_example(self, last_cought_word, line):
        new_line = []
        for word in line.split(' '):
            if last_cought_word[:-1].lower() in word.lower():
                new_line.append('...')
            else:
                new_line.append(word)
        return ' '.join(new_line)

    def start_game(self, player_name):
        if player_name and not re.search('\W+', player_name):
            self.player_name = player_name
            self.good_answers_num = 0
            #self.words = list()
            self.played_words = []
        else:
            raise ValueError('The given name does not have the right format!')

    def get_game_rounds(self):
        '''
        {'meaning': 'testvalue', 'answer': 'testword', 'choices': ('1','2','testword', '4')}
        '''
        while True:
            while True:
                answer = random.choice(self.words.keys())
                if answer not in self.played_words:
                    break
            while True:
                choices = [random.choice(self.words.keys()), random.choice(self.words.keys()), random.choice(self.words.keys())]
                if answer not in choices and not any(c in self.played_words for c in choices):
                    break
            choices.append(answer)
            random.shuffle(choices)
            self.played_words.append(answer)
            yield dict(answer = answer,
                        meaning = self.words[answer].get('meaning', ''),
                        example = self.words[answer].get('example', ''),
                        full_example = self.words[answer].get('full_example', ''),
                        choices = choices)

    def end_game(self):
        if self.player_name:
            self.results.append((self.player_name, self.good_answers_num))
            self._results.update_saved_results()
            del self.played_words[:]
            self.player_name = None
            self.good_answers_num = 0

    @property
    def results(self):
        return self._results.results

